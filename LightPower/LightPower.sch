EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ScrewConnector:Barrel_Jack_MountingPin J1
U 1 1 5D9CBB5E
P 3750 3800
F 0 "J1" H 3807 4025 50  0000 C CNN
F 1 "Barrel_Jack_MountingPin" H 3807 4026 50  0001 C CNN
F 2 "DCJack:BarrelJack_Horizontal" H 3800 3760 50  0001 C CNN
F 3 "~" H 3800 3760 50  0001 C CNN
	1    3750 3800
	1    0    0    -1  
$EndComp
$Comp
L ScrewConnector:Barrel_Jack_MountingPin J2
U 1 1 5D9CC318
P 3750 4400
F 0 "J2" H 3807 4625 50  0000 C CNN
F 1 "Barrel_Jack_MountingPin" H 3807 4626 50  0001 C CNN
F 2 "DCJack:BarrelJack_Horizontal" H 3800 4360 50  0001 C CNN
F 3 "~" H 3800 4360 50  0001 C CNN
	1    3750 4400
	1    0    0    -1  
$EndComp
$Comp
L ScrewConnector:Barrel_Jack_MountingPin J5
U 1 1 5D9CC460
P 4750 3800
F 0 "J5" H 4807 4025 50  0000 C CNN
F 1 "Barrel_Jack_MountingPin" H 4807 4026 50  0001 C CNN
F 2 "DCJack:BarrelJack_Horizontal" H 4800 3760 50  0001 C CNN
F 3 "~" H 4800 3760 50  0001 C CNN
	1    4750 3800
	-1   0    0    1   
$EndComp
$Comp
L ScrewConnector:Screw_Terminal_01x02 J3
U 1 1 5D9D0F7F
P 4200 3250
F 0 "J3" V 4118 3062 50  0000 R CNN
F 1 "Screw_Terminal_01x02" V 4073 3062 50  0001 R CNN
F 2 "ScrewConnector:TerminalBlock_bornier-2_P5.08mm" H 4200 3250 50  0001 C CNN
F 3 "~" H 4200 3250 50  0001 C CNN
	1    4200 3250
	0    -1   -1   0   
$EndComp
$Comp
L ScrewConnector:Screw_Terminal_01x02 J4
U 1 1 5D9D52C0
P 4300 4950
F 0 "J4" V 4218 4762 50  0000 R CNN
F 1 "Screw_Terminal_01x02" V 4173 4762 50  0001 R CNN
F 2 "ScrewConnector:TerminalBlock_bornier-2_P5.08mm" H 4300 4950 50  0001 C CNN
F 3 "~" H 4300 4950 50  0001 C CNN
	1    4300 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 4300 4200 4300
Wire Wire Line
	4200 4300 4200 4500
Wire Wire Line
	4200 4300 4200 3900
Wire Wire Line
	4200 3700 4050 3700
Connection ~ 4200 4300
Connection ~ 4200 3700
Wire Wire Line
	4450 3900 4200 3900
Connection ~ 4200 3900
Wire Wire Line
	4200 3900 4200 3700
Wire Wire Line
	4450 4500 4200 4500
Connection ~ 4200 4500
Wire Wire Line
	4200 4500 4200 4750
Wire Wire Line
	4200 3450 4200 3700
$Comp
L Power:GND #PWR02
U 1 1 5D9DE9CD
P 4050 4550
F 0 "#PWR02" H 4050 4300 50  0001 C CNN
F 1 "GND" H 4055 4377 50  0000 C CNN
F 2 "" H 4050 4550 50  0001 C CNN
F 3 "" H 4050 4550 50  0001 C CNN
	1    4050 4550
	1    0    0    -1  
$EndComp
$Comp
L Power:GND #PWR01
U 1 1 5D9DEC03
P 4050 3950
F 0 "#PWR01" H 4050 3700 50  0001 C CNN
F 1 "GND" H 4055 3777 50  0000 C CNN
F 2 "" H 4050 3950 50  0001 C CNN
F 3 "" H 4050 3950 50  0001 C CNN
	1    4050 3950
	1    0    0    -1  
$EndComp
$Comp
L Power:GND #PWR06
U 1 1 5D9DF6A8
P 4450 4250
F 0 "#PWR06" H 4450 4000 50  0001 C CNN
F 1 "GND" H 4455 4077 50  0000 C CNN
F 2 "" H 4450 4250 50  0001 C CNN
F 3 "" H 4450 4250 50  0001 C CNN
	1    4450 4250
	-1   0    0    1   
$EndComp
$Comp
L Power:GND #PWR05
U 1 1 5D9DF952
P 4450 3650
F 0 "#PWR05" H 4450 3400 50  0001 C CNN
F 1 "GND" H 4455 3477 50  0000 C CNN
F 2 "" H 4450 3650 50  0001 C CNN
F 3 "" H 4450 3650 50  0001 C CNN
	1    4450 3650
	-1   0    0    1   
$EndComp
$Comp
L ScrewConnector:Barrel_Jack_MountingPin J6
U 1 1 5D9CC59A
P 4750 4400
F 0 "J6" H 4807 4625 50  0000 C CNN
F 1 "Barrel_Jack_MountingPin" H 4807 4626 50  0001 C CNN
F 2 "DCJack:BarrelJack_Horizontal" H 4800 4360 50  0001 C CNN
F 3 "~" H 4800 4360 50  0001 C CNN
	1    4750 4400
	-1   0    0    1   
$EndComp
$Comp
L Power:GND #PWR03
U 1 1 5D9DFDE9
P 4300 3500
F 0 "#PWR03" H 4300 3250 50  0001 C CNN
F 1 "GND" H 4305 3327 50  0000 C CNN
F 2 "" H 4300 3500 50  0001 C CNN
F 3 "" H 4300 3500 50  0001 C CNN
	1    4300 3500
	1    0    0    -1  
$EndComp
$Comp
L Power:GND #PWR04
U 1 1 5D9DE393
P 4300 4700
F 0 "#PWR04" H 4300 4450 50  0001 C CNN
F 1 "GND" H 4305 4527 50  0000 C CNN
F 2 "" H 4300 4700 50  0001 C CNN
F 3 "" H 4300 4700 50  0001 C CNN
	1    4300 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 3500 4300 3450
Wire Wire Line
	4300 4700 4300 4750
Wire Wire Line
	4450 4300 4450 4250
Wire Wire Line
	4450 3700 4450 3650
Wire Wire Line
	4050 3900 4050 3950
Wire Wire Line
	4050 4550 4050 4500
$EndSCHEMATC
